<?php include 'Controller.php'; ?>
<?php $informations 	= curlJson("https://api.maedchenflohmarkt.de/v1/attributes.json"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>CURL</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
	<div class="jumbotron text-center">
	  <h1>GET JSON DATA</h1>
	  <p>USING CURL</p> 
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-success">
					<div class="panel-heading"><h2>Sizes</h2></div>
					<div class="body">
						<div class="table-responsive">
						  <table class="table table-striped">
						    <thead>
						    	<tr>
						    		<th>#</th>
						    		<th>Size ID</th>
						    		<th>name</th>
						    	</tr>
						    </thead>
						    <tbody>
						    	<?php $id 			= 0; ?>
						    	<?php $sizes 		= $informations['sizes']; ?>
						    	<?php for ($i=0; $i < count($sizes)-1; $i++) { 
						    		?>
						    			<tr>
						    				<td><?php echo $id+1; ?></td>
						    				<td><?php echo $sizes[$i]['sizeId']; ?></td>
						    				<td><?php echo $sizes[$i]['name']; ?></td>
						    			</tr>
						    		<?php
						    		$id++;
						    	}
						    	?>
						    </tbody>
						  </table>
						</div>
					</div>
				</div>
			</div>
			<br><br>
			<div class="col-md-12">
				<div class="panel panel-success">
					<div class="panel-heading"><h2>Colors</h2></div>
					<div class="body">
						<div class="table-responsive">
						  <table class="table table-striped">
						    <thead>
						    	<tr>
						    		<th>#</th>
						    		<th>Color ID</th>
						    		<th>Color Code</th>
						    		<th>Name</th>
						    	</tr>
						    </thead>
						    <tbody>
						    	<?php $id 			= 0; ?>
						    	<?php $colors 		= $informations['colors']; ?>
						    	<?php for ($i=0; $i < count($colors)-1; $i++) { 
						    		?>
						    			<tr>
						    				<td><?php echo $id+1; ?></td>
						    				<td><?php echo $colors[$i]['colorId']; ?></td>
						    				<td><?php echo $colors[$i]['color']; ?></td>
						    				<td><?php echo $colors[$i]['name']; ?></td>
						    			</tr>
						    		<?php
						    		$id++;
						    	}
						    	?>
						    </tbody>
						  </table>
						</div>
					</div>
				</div>
			</div>
			<br><br>
			<div class="col-md-12">
				<div class="panel panel-success">
					<div class="panel-heading"><h2>Conditions</h2></div>
					<div class="body">
						<div class="table-responsive">
						  <table class="table table-striped">
						    <thead>
						    	<tr>
						    		<th>#</th>
						    		<th>Condition ID</th>
						    		<th>Name</th>
						    	</tr>
						    </thead>
						    <tbody>
						    	<?php $id 			= 0; ?>
						    	<?php $conditions	= $informations['conditions']; ?>
						    	<?php for ($i=0; $i < count($conditions)-1; $i++) { 
						    		?>
						    			<tr>
						    				<td><?php echo $id+1; ?></td>
						    				<td><?php echo $conditions[$i]['conditionId']; ?></td>
						    				<td><?php echo $conditions[$i]['name']; ?></td>
						    			</tr>
						    		<?php
						    		$id++;
						    	}
						    	?>
						    </tbody>
						  </table>
						</div>
					</div>
				</div>
			</div>

			<!-- Product and Product Report -->
			<?php $fields			= $informations['fields']; ?>
			<?php $product 			= $fields['product']; ?>
			<?php $product_report 	= $fields['product_report']; ?>

			<br><br>
			<div class="col-md-12">
				<div class="panel panel-success">
					<div class="panel-heading"><h2>Product</h2></div>
					<div class="body">
						<div class="table-responsive">
						  <table class="table table-striped">
						    <thead>
						    	<tr>
						    		<th>#</th>
						    		<th>Button</th>
    								<th>Description</th>
    								<th>General</th>
    								<th>Images</th>
    								<th>Name</th>
    								<th>Payment & Shipping</th>
    								<th>Price</th>
    								<th>Size</th>
						    	</tr>
						    </thead>
						    <tbody>
						    		<tr>
					    				<td><?php echo $id+1; ?></td>
					    				<td><?php echo $product['button']['label']; ?></td>
					    				<td><?php echo "Type: ".$product['description']['type']; ?>
					    					<?php echo "<br>Length: ".$product['description']['length']['object']; ?>
					    						<?php echo "<br><br>Tip: "; ?>
					    						<?php echo "<br>Action: ".$product['description']['tip']['action']; ?>
					    						<?php echo "<br>Title: ".$product['description']['tip']['title']; ?>
					    					</td>
					    				<td></td>
					    				<td></td>
					    				<td></td>
					    				<td></td>
					    				<td></td>
					    				<td></td>
					    			</tr>
						    			
						    </tbody>
						  </table>
						</div>
					</div>
				</div>
			</div>
			<br><br>
			<div class="col-md-12">
				<div class="panel panel-success">
					<div class="panel-heading"><h2>Product Report</h2></div>
					<div class="body">
						<div class="table-responsive">
						  <table class="table table-striped">
						    <thead>
						    	<tr>
						    		<th>#</th>
						    		<th>Information</th>
						    	</tr>
						    </thead>
						    <tbody>
						    	<?php $id 			= 0; ?>
						    	<?php $information	= $informations['information']; ?>
						    	<?php for ($i=0; $i < count($information); $i++) { 
						    		?>
						    			<tr>
						    				<td><?php echo $id+1; ?></td>
						    				<td><?php echo $information['marketplaceCommission']; ?></td>
						    			</tr>
						    		<?php
						    		$id++;
						    	}
						    	?>
						    </tbody>
						  </table>
						</div>
					</div>
				</div>
			</div>

<!-- i just want to try how the history displayed to check the posible changes -->


			<br><br>
			<div class="col-md-12">
				<div class="panel panel-success">
					<div class="panel-heading"><h2>Information</h2></div>
					<div class="body">
						<div class="table-responsive">
						  <table class="table table-striped">
						    <thead>
						    	<tr>
						    		<th>#</th>
						    		<th>Information</th>
						    	</tr>
						    </thead>
						    <tbody>
						    	<?php $id 			= 0; ?>
						    	<?php $information	= $informations['information']; ?>
						    	<?php for ($i=0; $i < count($information); $i++) { 
						    		?>
						    			<tr>
						    				<td><?php echo $id+1; ?></td>
						    				<td><?php echo $information['marketplaceCommission']; ?></td>
						    			</tr>
						    		<?php
						    		$id++;
						    	}
						    	?>
						    </tbody>
						  </table>
						</div>
					</div>
				</div>
			</div>
			<br><br>
			<div class="col-md-12">
				<div class="panel panel-success">
					<div class="panel-heading"><h2>Materials</h2></div>
					<div class="body">
						<div class="table-responsive">
						  <table class="table table-striped">
						    <thead>
						    	<tr>
						    		<th>#</th>
						    		<th>Materials ID</th>
						    		<th>Name</th>
						    	</tr>
						    </thead>
						    <tbody>
						    	<?php $id 			= 0; ?>
						    	<?php $materials	= $informations['materials']; ?>
						    	<?php for ($i=0; $i < count($materials)-1; $i++) { 
						    		?>
						    			<tr>
						    				<td><?php echo $id+1; ?></td>
						    				<td><?php echo $materials[$i]['materialId']; ?></td>
						    				<td><?php echo $materials[$i]['name']; ?></td>
						    			</tr>
						    		<?php
						    		$id++;
						    	}
						    	?>
						    </tbody>
						  </table>
						</div>
					</div>
				</div>
			</div>
			<br><br>
			<div class="col-md-12">
				<div class="panel panel-success">
					<div class="panel-heading"><h2>Shipping Methods</h2></div>
					<div class="body">
						<div class="table-responsive">
						  <table class="table table-striped">
						    <thead>
						    	<tr>
						    		<th>#</th>
						    		<th>Shipping Method ID</th>
						    		<th>Name</th>
						    		<th>Default Price</th>
						    	</tr>
						    </thead>
						    <tbody>
						    	<?php $id 			= 0; ?>
						    	<?php $methods		= $informations['shippingMethods']; ?>
						    	<?php for ($i=0; $i < count($methods)-1; $i++) { 
						    		?>
						    			<tr>
						    				<td><?php echo $id+1; ?></td>
						    				<td><?php echo $methods[$i]['shippingMethodId']; ?></td>
						    				<td><?php echo $methods[$i]['name']; ?></td>
						    				<td><?php echo $methods[$i]['defaultPrice']; ?></td>
						    			</tr>
						    		<?php
						    		$id++;
						    	}
						    	?>
						    </tbody>
						  </table>
						</div>
					</div>
				</div>
			</div>
			<br><br>
			<div class="col-md-12">
				<div class="panel panel-success">
					<div class="panel-heading"><h2>Size Mapping</h2></div>
					<div class="body">
						<div class="table-responsive">
						  <table class="table table-striped">
						    <thead>
						    	<tr>
						    		<th>#</th>
						    		<th>Shipping Method ID</th>
						    		<th>Name</th>
						    	</tr>
						    </thead>
						    <tbody>
						    	<?php $id 			= 0; ?>
						    	<?php $sizemapping	= $informations['sizeMapping']; ?>
						    	<?php for ($i=0; $i < count($sizemapping)-1; $i++) { ?>
						    		<?php $data_size			= $sizemapping[$i]['sizes']; ?>
						    		<?php $count_size			= count($sizemapping[$i]['sizes']); ?>
						    		<?php $value_size			= $data_size[0]; ?>
						    		<?php if($count_size > 1){ ?>
						    			<?php $id_size 			= 0; ?>
						    			<?php foreach ($data_size as $key => $value): ?>
							    			<?php if ($id_size != 0) {
							    				$value_size 	= $value_size .", ". $value;
							    			}
							    			?>
							    			<?php $id_size++; ?>
						    			<?php endforeach ?>
						    		<?php } ?>
						    			<tr>
						    				<td><?php echo $id+1; ?></td>
						    				<td><?php echo $sizemapping[$i]['categoryId']; ?></td>
						    				<td><?php echo $value_size; ?></td>
						    			</tr>
						    		<?php $id++; ?>
						    	<?php } ?>
						    </tbody>
						  </table>
						</div>
					</div>
				</div>
			</div>


		</div>
	</div>
</body>
</html>